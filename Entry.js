import React from "react";
import {
	View,
	Text,
	ActivityIndicator,
	Platform,
	UIManager,
	LayoutAnimation
} from "react-native";

const Entry = props => {
	console.log("Hello => ", props);
	return (
		<Splash />
		// <View style={{ flex: 1 }}>
		// 	<Header style={{ flex: 0.15 }} />
		// 	<Content style={{ flex: 0.7 }} />
		// 	<Footer style={{ flex: 0.15 }} />
		// </View>
	);
};

export default Entry;

class Splash extends React.Component {
	constructor() {
		super();
		if (Platform.OS === "android") {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
		this.state = {
			loader: true
		};
	}

	componentDidMount() {
		setTimeout(() => {
			LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
			this.setState({ loader: false });
		}, 3000);
	}

	render() {
		return (
			<View
				style={{
					flex: 1
				}}
			>
				{this.state.loader ? (
					<ActivityIndicator />
				) : (
					<View
						style={{
							flex: 1,
							backgroundColor: "#000000",
							justifyContent: "center",
							alignItems: "center"
						}}
					>
						<View
							style={{
								width: 70,
								height: 70,
								borderRadius: 35,
								borderWidth: 3,
								justifyContent: "center",
								alignItems: "center",
								borderColor: "#ffffff"
							}}
						>
							<Text
								style={{
									color: "#ffffff",
									fontFamily: "verdana",
									fontSize: 30
								}}
							>{`i`}</Text>
						</View>
						<Text
							style={{ color: "#ffffff", fontFamily: "verdana", fontSize: 20 }}
						>{`Welcome to\nincredicodes`}</Text>
					</View>
				)}
			</View>
		);
	}
}

class Header extends React.Component {
	render() {
		console.log("Header");
		return (
			<View
				style={[{ flex: 1, backgroundColor: "#000000" }, this.props.style]}
			/>
		);
	}
}

class Content extends React.Component {
	componentDidMount() {
		this.setState({ a: 1 });
	}

	render() {
		console.log("Content");
		return (
			<View
				style={[{ flex: 1, backgroundColor: "#ffffff" }, this.props.style]}
			/>
		);
	}
}

class Footer extends React.Component {
	render() {
		console.log("Footer");
		return (
			<View
				style={[{ flex: 1, backgroundColor: "#000000" }, this.props.style]}
			/>
		);
	}
}
